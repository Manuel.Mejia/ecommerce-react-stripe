import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { fabClasses } from '@mui/material';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import accounting from 'accounting';
import { useStateValue } from '../StateProvider';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function Product(props) {
  let stars = new Array(props.product.rating);
  const {addBasket} = useStateValue();
  const [expanded, setExpanded] = React.useState(false);
  
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  
  const addToBasket = (e) => {
    e.preventDefault()
    addBasket(props.product);
  }


  return (
    <Card sx={{ width: "100%" }}>
      <CardHeader
       
        action={
          <Typography 
                className = {fabClasses.action}
                variant = "h5"
                color = "textSecondary">
                    {accounting.formatMoney(props.product.price)}
          </Typography>
        }
        title={props.product.productType}
        subheader="In stock"
      />
      <CardMedia
        component="img"
        height="194"
        image= {props.product.image}
        alt="nike"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {props.product.name}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to card" onClick={addToBasket}>
          <AddShoppingCartIcon  />
        </IconButton>
        {
        stars.fill().map((star,i) => (
               <p key = {i}>&#11088;</p>     
                    ))
                }
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
            <Typography>{props.product.description}</Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
