import * as React from 'react';
import style from '../moduleCss/CheckOutCard.module.css'
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { fabClasses } from '@mui/material';
import accounting from 'accounting'
import DeleteIcon from '@mui/icons-material/Delete';
import {useStateValue} from '../StateProvider';



export default function CheckOutCard(props) {
    let stars = new Array(props.product.rating);
    const {removeBasket} = useStateValue();

    const removeItem = (e) => {
        e.preventDefault();
        removeBasket(props.product.id);
    }

    return (
        <Card sx={{ width: "100%" }}>
            <CardHeader

                action={
                    <Typography
                        className={fabClasses.action}
                        variant="h5"
                        color="textSecondary">
                        {accounting.formatMoney(props.product.price)}
                    </Typography>
                }
                title={props.product.productType}
                subheader="In stock"
            />
            <CardMedia
                component="img"
                height="194"
                image={props.product.image}
                alt="nike"
            />
            <CardContent>
                <Typography variant="body2" color="text.secondary">
                    {props.product.name}
                </Typography>
            </CardContent>
            <CardActions disableSpacing >


                {
                    stars.fill().map((star, i) => (
                        <p key = {i}>&#11088;</p>
                    ))
                }
              <div className = {style.deleteCard}/>
                <IconButton className= {style.deleteCard} onClick = {removeItem}>
                    <DeleteIcon className= {style.deleteCard}    />
                </IconButton>             
                
            </CardActions>
        </Card>
    );
}
