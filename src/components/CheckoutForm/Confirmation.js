import { Button, Divider, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

export default function Confirmation({ message }) {
    return (
        <>
            <Typography variant="h6" >{message}</Typography>
            <Divider/>
            <Typography variant="subtitle1" sx = {{
                marginTop: "0.5rem"
            }}>{message == 'Payment Succefull'? 'Reference: uhaud23232': " "}</Typography>
            <Button component = {Link} to='/' color = "secondary" sx = {{
                marginTop: "1rem"
            }}>Back to Home</Button>
        </>

    )
}