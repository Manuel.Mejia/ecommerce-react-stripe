import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import AddressInput from './AdressInput';
import {useForm, FormProvider} from 'react-hook-form';
import { useStateValue } from '../../StateProvider';


export default function AddressForm({next}) {
  
  const {setShipping} = useStateValue();
  const methods = useForm();

  return (
    <>
      <Typography variant='h6' gutterBottom>
        Shipping Address
      </Typography>
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(data => {
                setShipping(data);
                next()})}>
        <Grid container spacing={3}>
          <AddressInput name="firstName" label="First Name" required/>
          <AddressInput name="LastName" label="Last Name" required/>
          <AddressInput name="address1" label="Address 1" required/>
          <AddressInput name="address2" label="Address 2" required/>
          <AddressInput name="city" label="City" required/>
          <AddressInput name="state" label="State/Province/Region" required/>
          <AddressInput name="zip" label="Zip / Postal Code" required/>
          <AddressInput name="country" label="Country" required/>
        </Grid>
        <div style={{display:"flex", justifyContent:'space-between', marginTop: "1rem", width:"100%"}}>
          <Button component={Link} to='/checkout' color='secondary'>Back to the checkout page</Button>          
          <Button type='submit' variant='contained' color='primary' >Next</Button>
        </div>
        </form>
        
      </FormProvider>
    </>
  );
}