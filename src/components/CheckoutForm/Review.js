import * as React from 'react';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import accounting from "accounting";

import { useStateValue } from '../../StateProvider';
import { getTotalCart } from '../../reducer';




export default function Review() {

  const {basket} = useStateValue();
  const  totalCart =  getTotalCart(basket);  

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Order summary
      </Typography>
      <List disablePadding>
        {basket?.map((product) => (
          <ListItem key={product.id} sx={{ py: 1, px: 0 }}>
            <ListItemText primary={product.productType} secondary={product.name} />
            <Typography variant="body2">{accounting.formatMoney(product.price)}</Typography>
          </ListItem>
        ))}

        <ListItem sx={{ py: 1, px: 0 }}>
          <ListItemText primary="Total" />
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {accounting.formatMoney(totalCart)}
          </Typography>
        </ListItem>
      </List>      
    </React.Fragment>
  );
}