import { Button, Divider, Typography } from '@mui/material';
import * as React from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import Review from './Review';
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { getTotalCart } from '../../reducer';
import { useStateValue } from '../../StateProvider';
import accounting from "accounting";
import axios from 'axios';


const stripePromise = loadStripe("pk_test_51LS82vE4C4r4ufOUk4YPyw7jR0fGk9XbNF8hAqszvvQW9SOkeE7jdaIBYT8TVTZx3iUpOlqzjRU4wItnTkbkxbWV00CctFYG58")

const CheckoutForm = ({ back, next }) => {

  const { basket, confirmMessage } = useStateValue();
  const totalCart = getTotalCart(basket);
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async (e) => {
    e.preventDefault();

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement)
    })

    try {
      const { id } = paymentMethod;
      const { data } = await axios.post("http://localhost:3001/api/checkout", {
        id,
        amount: totalCart * 100
      });

      confirmMessage(data.message);
      next()
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <>
      <form style={{ marginTop: "20px" }} onSubmit={handleSubmit}>
        <CardElement options={{ fontSize: "25px" }} />
        <div style={{ display: "flex", justifyContent: "space-between", marginTop: "1rem" }}>
          <Button type="submit" onClick={back} color='secondary'>Back</Button>
          <Button type="submit" onSubmit={next} variant='contained' color='primary'>Pay {accounting.formatMoney(totalCart)} </Button>
        </div>
      </form>
    </>
  )
}

export default function PaymentForm({ next, back }) {

  return (
    <>
      <Review />
      <Divider />
      <Typography variant="h6" gutterBottom style={{ marginTop: "1rem" }}>
        Payment method
      </Typography>
      <Elements stripe={stripePromise}>
        <CheckoutForm back={back} next={next} />
      </Elements>
    </>
  );
}