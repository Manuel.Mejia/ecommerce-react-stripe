import { Grid, TextField } from "@mui/material";
import React from "react";
import {useFormContext, Controller} from 'react-hook-form';

export default function AddressInput({name, label, required}){
    const {control}= useFormContext();
    return(
        <Grid item xs={12} sm={6}>
            <Controller 
                control={control}
                fullWidth
                defaultValue="" 
                name= {name}                
                render={({field}) => (
                    <TextField {...field} label={label} required={required} fullWidth variant="standard" />
                )}/>
        </Grid>
    )
}