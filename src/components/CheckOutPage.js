import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import style from '../moduleCss/CheckOutPage.module.css';
import CheckOutCard from './CheckOutCard';
import Total from './Total';
import { useStateValue } from '../StateProvider';


export default function CheckOutPage() {

    const { basket } = useStateValue()
    const FormRow = () => {
        return (
            <>
                {basket?.map(item => (
                    <Grid item xs={12} sm={8} md={6} lg={4}>
                        <CheckOutCard Key={item.id} product={item} style={{width:"100%"}} />
                    </Grid>
                ))

                }
            </>
        )
    }

    return (
        <Box sx={{ flexGrow: 1 }} className={style.checkOutPage}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography align='center' gutterBottom variant='h6'>
                        Shopping Cart
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={8} md={9} container spacing={2}>
                    <FormRow />
                </Grid>
                <Grid item xs={12} sm={4} md={3}>
                    <Typography align='center' gutterBottom variant='h4'>
                        <Total />
                    </Typography>
                </Grid>
            </Grid>
        </Box>
    );
}
