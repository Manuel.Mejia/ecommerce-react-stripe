import * as React from 'react';
import style from '../moduleCss/Navbar.module.css'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import Badge from '@mui/material/Badge';
import logo from '../assets/logo.png';
import { useNavigate } from 'react-router-dom';
import { useStateValue } from '../StateProvider';
import { auth } from '../firebase';

export default function Navbar() {
  const navigate = useNavigate();

  const {
    basket,
    sign,
    user,
    changeSign,
    setUser,
    emptyBasket } = useStateValue();

  const userLog = (e) => {
    e.preventDefault();

    if (!user) {
      navigate('/signin')

    } else if (user) {

      auth.signOut();
      changeSign(2);
      emptyBasket([]);
      setUser(null);
      navigate('/');
      
    }
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" className={style.navbar}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => { navigate('/') }}
          >
            <img src={logo} className={style.image} />
          </IconButton>
          <Typography className={style.userName} component="div" >
            Hello {user ? user.email : 'Guest'}
          </Typography>
          <div className={style.button}>
            <IconButton className={style.sign} onClick={userLog} color="inherit" sx={{ fontSize: "18px" }}>
              {sign}
            </IconButton>

            <IconButton aria-label='show cart items' color='inherit' onClick={(e) => {
              e.preventDefault()
              navigate('/checkout')
            }}>
              <Badge badgeContent={basket?.length} color="success">
                <ShoppingCartIcon fontSize='medium' />
              </Badge>

            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
