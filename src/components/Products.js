import style from '../moduleCss/Products.module.css';
import Grid from '@mui/material/Grid';
import Product from './Product';
import products from '../productsData';

export default function Products() {
    return (
        <div className={style.gridContainer}>
            <Grid container spacing={2}  className={style.gridContainer} >
                {
                    products.map((product,i) => (
                        <Grid item xs={12} sm={6} md={4} lg={3} className={style.gridProduct}>
                            <Product key={product.id} product={product} className = {style.product}/>
                        </Grid>
                    ))
                    }

            </Grid>
        </div>
    )
}