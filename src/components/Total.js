import React from "react";
import accounting from "accounting";
import style from '../moduleCss/Total.module.css';
import Button from '@mui/material/Button';
import { getTotalCart } from '../reducer';
import { useStateValue } from '../StateProvider';
import { useNavigate } from "react-router-dom";

export default  function Total() {
    const {basket } = useStateValue();
    const navigate = useNavigate();
    const  totalCart =  getTotalCart(basket);  

    const check = (e) => {
        e.preventDefault();
        navigate('/checkoutpay');
    }
   
    return (
        <div className={style.container}>
            <h6>Total a pagar:{basket?.length}</h6>
            <h4>{accounting.formatMoney(totalCart)}</h4>
            <Button  className = {style.button} variant="contained" color="success" onClick={check}>
                Check out
            </Button>
        </div>
    )
}