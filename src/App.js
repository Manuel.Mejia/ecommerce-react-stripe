import './App.css';
import { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';

import Navbar from './components/Navbar';
import Products from './components/Products';
import CheckOutPage from './components/CheckOutPage';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Checkout from './components/CheckoutForm/Checkout';

import { useStateValue } from './StateProvider'
import { auth } from './firebase';



function App() {
  const {setUser} = useStateValue();

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      
      if(authUser){
        setUser(authUser);
      }

    })
  }, [])

  return (
    <div className="App">
      
        <Navbar />              
        <Routes>
          <Route path= "/" element= { <Products /> } exact />
          <Route path= "/checkout" element= { <CheckOutPage /> } exact/>
          <Route path= "/signin" element= { <SignIn/> } exact/>
          <Route path= '/signup' element= { <SignUp/> } />
          <Route path= '/checkoutpay' element= { <Checkout/> } />
        </Routes>
      
    </div>
  );
}

export default App;
