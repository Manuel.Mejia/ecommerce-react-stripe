export const initialState = {
    basket: [],
    sign: "Sing In",
    user: null,
    shippingData:[],
    confirmationMessage: null
}

export const getTotalCart = (basket) => {
    return basket?.reduce((amount,item) => amount + item.price, 0)
}

export default function reducer(state, action){
   
    switch(action.type){
        case 'ADD_TO_BASKET':

            return {
                ...state,
                basket: [...state.basket, action.payload]
            }
            
        case 'REMOVE_TO_BASKET':

            const index = state.basket.findIndex((basketItem => basketItem.id === action.payload));
            const newBasket = [...state.basket];

            if(index >= 0){
                newBasket.splice(index, 1)
            }else{
                console.log('cant remove product')
            }   
            
            return {
                ...state,
                basket: newBasket
            }
        case 'ADD_TO_SIGN':

            return {
                ...state,
                sign: action.payload
            }
        case 'SET_TO_USER':

            return{
                ...state,
                user: action.payload
            }
        case 'EMPTY_BASKET':

            return{
                ...state,
                basket: action.payload
            }
        case 'SET_SHIPPING':
            return{
                ...state,
                shippingData:[...state.shippingData, action.payload]
            }
        case 'CONFIMATION_PAY':
            return{
                ...state,
                confirmationMessage: action.payload
            }
        default: return state;
            
    }
   
}