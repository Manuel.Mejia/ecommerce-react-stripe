// Import the functions you need from the SDKs you need

import  firebase  from  'firebase/compat/app' ; 
import  'firebase/compat/auth' ; 

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBnuMnb9SxI6-vpy_WAK18NqdkzBlpbenY",
  authDomain: "ecommerce-react-846dc.firebaseapp.com",
  projectId: "ecommerce-react-846dc",
  storageBucket: "ecommerce-react-846dc.appspot.com",
  messagingSenderId: "208212309398",
  appId: "1:208212309398:web:cba9044b44af605f5c59b7",
  measurementId: "G-GZ1L0DXZ4Y"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);


const auth = firebase.auth();

export {auth}