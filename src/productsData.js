const products = [
    {
        id: 1,
        productType:"Shoes",
        name:"Nike Air Running shoes",
        price:50,
        rating: 4,
        image: "https://i8.amplience.net/i/jpl/nike-am97-white-99afddd86ead453d0e905301dc72a89d",
        description:"The Nike Air Force 1 remains a popular retro shoe to this day. The Air Force 1 features an upper that is most frequently dressed in leather, a large Swoosh across the lateral and medial sides, perforated toe boxes, metal lace dubraes, and an Air-encapsulated midsole with signature “AIR” text near the rear."

    },
    {
        id: 2,
        productType:"Sweater ",
        name:"Hugo Boss",
        price:150,
        rating: 5,
        image: "https://shop.sax.com.py/uploads/product/1600194624_1600194624.jpg",
        description:"hugo boss black sweater for men, the best style you can have at a great price."

    },
    {
        id: 3,
        productType:"Jean",
        name:"Dsquared2",
        price:400,
        rating: 4,
        image: "https://cdn.shopify.com/s/files/1/2026/4609/products/WhatsAppImage2021-01-25at3.14.03PM_7_2048x2048.jpg?v=1612035857",
        description:"Dark London Calling Wash Skater Jeans."

    },
    {
        id: 4,
        productType:"Glasses",
        name:"Ray ban originals",
        price:100,
        rating: 4,
        image: "https://images.ray-ban.com/is/image/RayBan/805289126607__STD__shad__qt.png?impolicy=RB_Product&width=1024&bgc=%23f2f2f2",
        description:"If you are looking for a classic signature, with more than proven quality, Ray-Ban is among the best sunglasses brands and, without a doubt, it is a perfect choice for you. Its name means ,lightning barrier,and the truth is that it could not better describe what it offers."

    }

]

export default products;