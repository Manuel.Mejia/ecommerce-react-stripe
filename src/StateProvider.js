import {createContext, useReducer, useContext} from 'react';
import reducer, { initialState } from './reducer';


export const StateContext = createContext();

export const StateProvider = ({children}) => {
    const [state, dispatch ] = useReducer(reducer, initialState);

    const addBasket = (product) => {        
        dispatch({
            type: 'ADD_TO_BASKET',
            payload: product
        });
    }

    const removeBasket = (id) => {        
        dispatch({
            type: 'REMOVE_TO_BASKET',
            payload: id
        });
    }

    const getTotalCart = (basket) => {        
        return basket?.reduce((amount,item) => amount + item.price, 0)
    }

    const changeSign = (change) => {
        let actionType = 'ADD_TO_SIGN';

        if(change === 1){
            dispatch({
                type: actionType,
                payload: 'Sign up'
            });
        }else if (change === 2){
            dispatch({
                type: actionType,
                payload: 'Sing in'
            });
        }else if (change === 3){
            dispatch({
                type: actionType,
                payload: 'Sign Out'
            });
        }
    }
    const setUser = (user) => {
        dispatch({
            type: 'SET_TO_USER',
            payload: user
        });
    }

    const emptyBasket = (basket) => {
        dispatch({
            type: 'EMPTY_BASKET',
            payload: basket
        });
    }

    const setShipping = (data) => {
        dispatch({
            type: 'SET_SHIPPING',
            payload: data
        })
        
    }

    const confirmMessage = (mess) =>{
        dispatch({
            type: 'CONFIMATION_PAY',
            payload: mess
        })
    }
    
    return <StateContext.Provider value={{
        ...state, 
        addBasket,
        removeBasket,
        getTotalCart,
        changeSign,
        setUser,
        emptyBasket,
        setShipping,
        confirmMessage
        }}>

        {children}
    </StateContext.Provider>
}

export default StateProvider;

export const useStateValue = () => useContext(StateContext)