'use strict'
require('dotenv').config()
const express = require('express');
const Stripe = require('stripe');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

//middlewares
app.use(cors({
    origin:"http://localhost:3000"
}));
app.use(morgan('dev'));
app.use(express.json());

//settings
app.set('port', 3001 || process.env.PORT);

//routes
app.use('/api', require('./routes/data.route'));

//server
app.listen(app.get('port'), () => {
    console.log(`Serven on port ${app.get('port')}`);
})
