
const {Router} = require('express');
const Stripe = require('stripe');

const stripe = Stripe(process.env.STRIPE_KEY);
const router = Router();

router.post('/checkout', async (req, res) => {
   
    const {id, amount} = req.body;

     try {
        const paymentIntent = await stripe.paymentIntents.create({
            amount,
            currency: 'usd',
            payment_method_types: ['card'],
            statement_descriptor: 'Custom descriptor',
          });

        console.log(paymentIntent);
        return res.status(200).json({
            message: 'Payment Succefull'
        })

     } catch (error) {
      console.log(error)
        return res.json({
          message: "Payment invalid, your card is ivalid"
        })
     }
})


module.exports = router;